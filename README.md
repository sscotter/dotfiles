# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ##

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## How do I get set up? ##

* Summary of set up
This repo contains a collection of my personalised RC files from various machines.
In order to get a consistent experience across all the machines I maintain my dotfiles here and sync changes down.

* Dependencies
	* rcm
	* git

* Deployment instructions
	* su 
	* pkg install git rcm
	* exit (from root)
	* git clone https://sscotter@bitbucket.org/sscotter/dotfiles.git ~/.dotfiles
	* rcup -v -x "README.md contributors.txt"

## Making changes ##
* Adding a RC file to the system 
	* create ~/.some_rc_file with the desired contents
	* mkrc ~/.tigrc (copies it to ~/.dotfiles and symlinks it to ~/)
	* cd ~/.dotfiles
	* git status
	* git add .some_rc_file
	* git status
	* git commit -m 'Some commit message here'
	* git push -u origin master
	The central repo is now up to date

* Modifying an existing RC file
	* cd ~/.dotfiles
	* git status
	* git commit -a -m 'Some commit message here'
	* git push -u origin master
	The central repo is now up to date

* Updating the local version of the repo
	* cd ~/.dotfiles
	* git pull origin master

## References ##
[rc file (dotfile) management](https://github.com/thoughtbot/rcm)
[rcm - dotfile management](http://thoughtbot.github.io/rcm/rcm.7.html)