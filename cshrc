#
# Fetch the latest version from http://freebsd.stephen-scotter.net/personalizations/.cshrc
#
# Version 2013-01-05-0931
#
# Notes :
# My default .cshrc.
# Adds confirmations to mv and rm commands
#
#
###############################################################################
#
# Place this file in the home directory of the user whom you log in as primarily, ie NOT root.
#
###############################################################################

alias h     history 25
alias j     jobs -l
alias la ls -aGh
alias lf ls -FAGh
alias ll ls -lAGh
alias grep  grep --color -n
alias mv    mv -i
alias rm    rm -i
alias cd 		'cd \!*;set prompt="[`whoami`@`hostname -f` $cwd]# "'
alias tail 	/usr/bin/tail -n 200
alias portsoutofdate '/usr/sbin/pkg version -vIL='
alias portsupdateall '/usr/sbin/portsnap fetch update && /usr/local/sbin/portmaster -daG'
alias pkgsoutofdate '/usr/sbin/pkg version -vRL='
alias pkgsvulnerable '/usr/sbin/pkg audit'

# A righteous umask
umask 22

set path = (/sbin /bin /usr/sbin /usr/bin /usr/games /usr/local/sbin /usr/local/bin $HOME/bin)

setenv   EDITOR nano
setenv   PAGER more
setenv   BLOCKSIZE K

if ($?prompt) then
   # An interactive shell -- set some stuff up

   # set promptchars = "%#"

   # screen(1) title-string escape sequence
   if ( $term != dumb ) then
      set title_esc='\ek\e\\'
   else
      set title_esc
   endif

   set tc_bold       = `echotc md`
   set tc_red        = `echotc AF 1`
   set tc_green      = `echotc AF 2`
   set tc_yellow     = `echotc AF 3`
   set tc_reset_attr = `echotc me`

   set promptchars = "%#"
   # set prompt = "%{$tc_bold%}%{$tc_red%}%N%{$tc_yellow%}@%{$tc_green%}%m %{$tc_bold%}%{$tc_red%}(%{$tc_yellow%}%~%{$tc_red%})%{$tc_green$title_esc%}%#%{$tc_reset_attr%} "

   if ($?WINDOW) then
      # set prompt="[`whoami`@`hostname -f`($WINDOW) $cwd]# "
		set prompt = "%{$tc_bold%}%{$tc_red%}%N%{$tc_yellow%}@%{$tc_green%}%m %{$tc_bold%}%{$tc_red%}($WINDOW)(%{$tc_yellow%}%~%{$tc_red%})%{$tc_green$title_esc%}%#%{$tc_reset_attr%} "
   else
      # set prompt="[`whoami`@`hostname -f`(-) $cwd]# "
		set prompt = "%{$tc_bold%}%{$tc_red%}%N%{$tc_yellow%}@%{$tc_green%}%m %{$tc_bold%}%{$tc_red%}(NO SCREEN)(%{$tc_yellow%}%~%{$tc_red%})%{$tc_green$title_esc%}%#%{$tc_reset_attr%} "
   endif

    # Substitute the filename to be completed when I type an <TAB> at the command line
   set filec

   # Do autolisting of commands while completing.
   set autolist

   # Do autocorrection while completing...
   # It does some very rudimentary corrections. I was not disatisfied yet.
   # set autocorrect

   # Use the history of commands to aid expansion.
   # Found in new versions of tcsh (at least 6.10)
   set autoexpand

   # Enabled "complete" to "enhance".
   # If you have a file called "complete.tcsh" and you
   # want to edit it, do "vi c.t<TAB>" and that's it.
   # set complete=enhance

   # Possible values : cmd complete all
   # Put it to extreme auto.
   # set correct=all

   # After a 'Ctrl-Z', it lists all the jobs.
   set listjobs

   # If the exit value is non-zero, print it.
   # Very handy when you do scripting.
   set printexitvalue

    # Ask for confirmation when 'rm *'.
   set rmstar

   # Remember my 100 most recent events
   set history = 100

   # Save the most recent 100 events when I log out
   set savehist = 100
   # Don't overwrite existing files with the redirection character ">"
   set noclobber

   # Notify me when the status of background jobs change
   set notify

   set mail = (/var/mail/$USER)

   if ( $?tcsh ) then
      bindkey "^W" backward-delete-word
      bindkey -k up history-search-backward
      bindkey -k down history-search-forward
      bindkey "^[[1~" beginning-of-line
      bindkey "^[[4~" end-of-line
      bindkey "^[[3~" delete-char
   endif
endif
