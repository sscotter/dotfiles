# $FreeBSD: release/9.0.0/share/skel/dot.login 190477 2009-03-27 21:13:14Z ru $
#
# .login - csh login script, read by login shell, after `.cshrc' at login.
#
# see also csh(1), environ(7).
#

if ( $TERM != "screen" ) then
   screen -RD
   #  -D -R   Attach here and now. In detail this means: If a session is run-
   #            ning,  then  reattach.  If necessary detach and logout remotely
   #            first.  If it was not running create it and  notify  the  user.
   #            This is the author's favorite.
   echo "Not in screen - Attempting to reattach, or create if not already running"
   echo "term=$TERM"
else
   echo "Already in screen - No need to do anything"
   echo "term=$TERM"
endif

if ( -x /usr/games/fortune ) /usr/games/fortune freebsd-tips

# Must leave at least one blank line on this end of this file if not the last command doesn't get executed
